extern crate actix_web;

use actix_web::{fs, server, App};

fn main() {
    println!("Started http server: 127.0.0.1:3030");
    server::new(|| {
        App::new()
	        .handler(
                "/",
                fs::StaticFiles::new("./static/").unwrap().index_file("index.html")
            )
    }).bind("127.0.0.1:3030")
        .expect("Can not start server on given IP/Port")
        .run();

}
